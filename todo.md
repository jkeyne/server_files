# Main tasks
- [x] Auto install docker
- [x] Install main services
    - [x] caddy (or traefik)
    - [x] vaultwarden
    - [x] invidious
    - [x] bookstack
    - [x] onedev
    - [ ] keycloak (maybe just kanidm)
    - [x] lldap
    - [ ] forward_auth
    - [x] searxng
    - [ ] joplin?
    - [x] fasten
- [x] Install media services
- [ ] Install paperless
- [ ] Install nextcloud

# Extra credit
- [ ] Pull info from OVH
- [ ] Configure DNS records

# Investigate
- [ ] Switch to kubernetes
    - Need storage solution
    - Need ingress solution
    - Need a way to manage lots of helm charts
- [ ] Look at https://github.com/nxthat/nanocl
- [ ] Look into openshift

# Far off
- [ ] Think about terraform
    - or maybe not, with the recent news
- [ ] Write some rust code for deploying services
    - i dunno about this one
    - but infra from code would be neat
- [ ] Write custom media server
    - needs to search a bunch of torrent sites
    - needs to automatically parse the torret file names
    - then send it to a torrent server
        - maybe see if i could integrate [synapse](https://github.com/Luminarys/synapse)
