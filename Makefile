tags = ""

all:
	DOCKER_TIMEOUT=600 ansible-playbook -i hosts.ini --ask-vault-pass playbook.yaml

tag:
	DOCKER_TIMEOUT=600 ansible-playbook -i hosts.ini --ask-vault-pass playbook.yaml --tags ${tags}

clean:
	DOCKER_TIMEOUT=600 ansible-playbook -i hosts.ini clean.yaml

migrate: stop
	ANSIBLE_STDOUT_CALLBACK=unixy ansible-playbook -i hosts.ini migrations/old_storage.yaml

stop:
	DOCKER_TIMEOUT=600 ansible-playbook -i hosts.ini -e stop_only=true clean.yaml

galaxy:
	ansible-galaxy install -r requirements.yaml
	pipx inject ansible sonarr-py
	pipx inject ansible radarr-py

plan:
	ansible-playbook -C -i hosts.ini --ask-vault-pass playbook.yaml

lint:
	@sed -i 's/secrets.yaml/secrets_template.yaml/g' playbook.yaml
	ansible-lint --show-relpath
	@sed -i 's/secrets_template.yaml/secrets.yaml/g' playbook.yaml

renovate:
	docker run --rm \
	  -e RENOVATE_PLATFORM=gitlab \
	  renovate/renovate
