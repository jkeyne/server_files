from __future__ import (absolute_import, division, print_function)
import requests
from ansible.module_utils.basic import AnsibleModule
import re
__metaclass__ = type

DOCUMENTATION = r'''
---
module: jackett_info

short_description: This gets a list of indexers from jackett

version_added: "1.0.0"

description: This gets a list of indexers from jackett

options:
    base_url:
        description: The base URL to access jackett
        required: true
        type: str
    api_key:
        description: Jackett's API key
        required: true
        type: str

author:
    - Jericho Keyne (@shadowwolf899)
'''

EXAMPLES = r'''
# Pass in a message
- name: Get jackett info
  dev.jkeyne.jackett_info:
    base_url: https://jackett.jkeyne.dev
    api_key: thisisanapikey
'''

RETURN = r'''
indexers:
    description: The list of indexer Torznab URLs
    type: str
    returned: successful
    sample:
        - https://jackett.jkeyne.dev/api/v2.0/indexers/internetarchive/results/torznab/
'''


def run_module():
    module_args = dict(
        base_url=dict(type='str', required=True),
        api_key=dict(type='str', required=True),
    )
    result = dict(
        changed=False,
        indexers=[],
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    url = "{}/api/v2.0/indexers/all/results/torznab/".format(
        module.params['base_url']
    )
    r = requests.get(url, {
        "t": "indexers",
        "configured": True,
        "apikey": module.params['api_key'],
    })
    pat = re.compile(r'indexer id="([^"]+)"')
    for res in pat.finditer(r.text):
        indexer_id = res.group(1)
        url = "{}/api/v2.0/indexers/{}/results/torznab/".format(
            module.params['base_url'],
            indexer_id,
        )
        result['indexers'].append(url)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
